variable "env" {
  default = "stage"
}

variable "folder_id" {
  description = "Yandex Cloud Folder ID where resources will be created"
  default = "b1ga5s3k1alml6au8c32"
}

variable "cloud_id" {
  description = "Yandex Cloud ID where resources will be created"
  default = "b1gi4rfq1lfkt0a0hts8"
}

variable "region" {
  description = "Yandex Cloud ID where resources will be created"
  default = "ru-central1-c"
}