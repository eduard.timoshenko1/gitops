// Create network for cloud
module "vpc" {
  source  = "../modules/vpc"
  cidr    = ["10.200.0.0/16"]
  env     = var.env
  region  = var.region
  folder_id = var.folder_id
}

//Create service account for kubernetes
module "sa" {
  source = "../modules/sa"
  env     = var.env
  region  = var.region
  folder_id = var.folder_id
}

module "kubernetes" {
  source = "../modules/kubernetes"
  env     = var.env
  region  = var.region
  folder_id = var.folder_id
  network_id = module.vpc.yandex_vpc_network
  service_account_id = module.sa.service_account_id
  yandex_vpc_subnet_id = module.vpc.yandex_vpc_subnet_id
}

module "nodes_workers" {
  source = "../modules/kubernetes_node_group"
  kubernetes_cluster_id = module.kubernetes.kubernetes_cluster_id
  env     = var.env
  region  = var.region
  group_name = "workers"
  ver = "1.18"
  yandex_vpc_subnet_id = module.vpc.yandex_vpc_subnet_id
}

module "nodes_ingress" {
  source = "../modules/kubernetes_node_group"
  kubernetes_cluster_id = module.kubernetes.kubernetes_cluster_id
  env     = var.env
  region  = var.region
  group_name = "ingress"
  ver = "1.18"
  yandex_vpc_subnet_id = module.vpc.yandex_vpc_subnet_id
  taints = ["node.role/ingress=:NoSchedule"]
  labels = {
    "node.role/ingress" = "true"
  }
}

module "helm_chart_ingress" {
  source = "../modules/helm_chart"
  chart_name = "ingress-nginx"
  chart_repo  = "https://kubernetes.github.io/ingress-nginx"
  chart_namespace = "ingress"
  values = [
    "${file("ingress.yaml")}"
  ]
  ver = "3.30.0"
  depends = [module.nodes_ingress, module.nodes_workers]
}

module "helm_chart_prometheus" {
  source = "../modules/helm_chart"
  chart_name = "kube-prometheus-stack"
  chart_repo  = "https://prometheus-community.github.io/helm-charts"
  chart_namespace = "prometheus"
  values = [
    "${file("prometheus.yaml")}"
  ]
  ver = "15.4.4"
  depends = [module.nodes_workers]
}