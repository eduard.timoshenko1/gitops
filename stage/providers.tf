terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.57.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.1.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.1.2"
    }
  }
  backend "http" {
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
    address= "https://gitlab.com/api/v4/projects/26402167/terraform/state/gitops"
    lock_address= "https://gitlab.com/api/v4/projects/26402167/terraform/state/gitops/lock"
    unlock_address= "https://gitlab.com/api/v4/projects/26402167/terraform/state/gitops/lock"

  }
}

provider "yandex" {
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.region
}


provider "kubernetes" {
  host = module.kubernetes.kubernetes_cluster_endpoint
  cluster_ca_certificate = module.kubernetes.kubernetes_cluster_ca
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command = "/tmp/yc"
    args = [
      "managed-kubernetes",
      "create-token",
    ]
  }
}

provider "helm" {
  kubernetes {
    host = module.kubernetes.kubernetes_cluster_endpoint
    cluster_ca_certificate = module.kubernetes.kubernetes_cluster_ca
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command = "/tmp/yc"
      args = [
        "managed-kubernetes",
        "create-token",
      ]
    }
  }
}


