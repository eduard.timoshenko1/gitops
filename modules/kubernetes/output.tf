output "instance_ips" {
  value = yandex_kubernetes_cluster.kub.master[0].cluster_ca_certificate
}

output "instance_ips2" {
  value = yandex_kubernetes_cluster.kub.master[0].external_v4_endpoint
}

output "kubernetes_cluster_id" {
  value = yandex_kubernetes_cluster.kub.id
}

output "kubernetes_cluster_endpoint" {
  value = yandex_kubernetes_cluster.kub.master[0].external_v4_endpoint
}

output "kubernetes_cluster_ca" {
  value = yandex_kubernetes_cluster.kub.master[0].cluster_ca_certificate
}