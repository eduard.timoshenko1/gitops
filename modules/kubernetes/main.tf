terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.57.0"
    }
  }
}

resource "yandex_kubernetes_cluster" "kub" {
  name        = "kub-${var.env}"
  network_id = var.network_id

  master {
    zonal {
      zone      = var.region
      subnet_id = var.yandex_vpc_subnet_id
    }
    version   = var.ver
    public_ip = true
  }
  release_channel = "RAPID"
  network_policy_provider = "CALICO"

  node_service_account_id = var.service_account_id
  service_account_id      = var.service_account_id
}