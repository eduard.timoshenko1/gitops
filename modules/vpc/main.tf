terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.57.0"
    }
  }
}
resource "yandex_vpc_network" "internal" {
  name = "${var.env}-vpc"
  folder_id      = var.folder_id
}

resource "yandex_vpc_subnet" "subnet" {
  zone           = var.region
  network_id     = yandex_vpc_network.internal.id
  v4_cidr_blocks = var.cidr
  name           = "${var.env}-subnet"
  folder_id      = var.folder_id
}