variable "region" {
  default = "ru-central1-a"
}

variable "env" {
  default = "test"
}

variable "cidr" {
  default = [
    "10.200.10.0/16"
  ]
}

variable "folder_id" {
  description = "Yandex Cloud Folder ID where resources will be created"
  default = "b1gfcfohvqclk8467fuh"
}