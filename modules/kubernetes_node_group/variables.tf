variable "folder_id" {
  description = "Yandex Cloud Folder ID where resources will be created"
  default = "b1gfcfohvqclk8467fuh"
}

variable "region" {
  default = "ru-central1-a"
}

variable "env" {
  default = "test"
}

variable "kubernetes_cluster_id" {
}

variable "group_name" {
}

variable "ver" {
}

variable "yandex_vpc_subnet_id" {
}

variable "taints" {
  default = []
}

variable "labels" {
  default = {}
}

variable "memory" {
  default = 2
}

variable "core" {
  default = 2
}

variable "disk_type" {
  default = "network-hdd"
}

variable "disk_size" {
  default = 64
}

variable "node_min" {
  default = 1
}

variable "node_max" {
  default = 2
}

variable "node_init" {
  default = 1
}