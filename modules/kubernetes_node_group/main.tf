terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.57.0"
    }
  }
}

resource "yandex_kubernetes_node_group" "group" {

  cluster_id  = var.kubernetes_cluster_id
  name        = var.group_name
  version     = var.ver
  node_taints = var.taints
  node_labels = var.labels

  instance_template {
    platform_id = "standard-v2"
    network_interface {
      nat                = true
      subnet_ids         = [var.yandex_vpc_subnet_id]
      security_group_ids = []
    }
    resources {
      core_fraction = 20
      memory        = var.memory
      cores         = var.core
    }

    boot_disk {
      type = var.disk_type
      size = var.disk_size
    }

    scheduling_policy {
      preemptible = false
    }
  }

  scale_policy {
    auto_scale {
      min = var.node_min
      initial = var.node_init
      max = var.node_max
    }
  }

  allocation_policy {
    location {
      zone = var.region
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = true
  }
}
