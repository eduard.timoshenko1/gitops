variable "chart_name" {}
variable "chart_repo" {}
variable "chart_namespace" {}
variable "values" {}
variable "ver" {}
variable "depends" {
  type    = any
  default = []
}