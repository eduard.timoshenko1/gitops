resource "helm_release" "helm" {
  name       = var.chart_name
  repository = var.chart_repo
  chart      = var.chart_name
  namespace  = var.chart_namespace
  version = var.ver
  atomic     = true
  force_update = false
  create_namespace = true

  values = var.values
  depends_on = [var.depends]
}